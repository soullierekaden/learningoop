class Cars:
    def __init__(self, speed, mileage, colour):
        self.speed = speed
        self.mileage = mileage
        self.colour = colour

class Chevy(Cars):
    speed = 150
    mileage = 40_000
    colour = "yellow"

class Dodge(Cars):
    speed = 160
    mileage = 60_000
    colour = "red"

class Ford(Cars):
    speed = 140
    mileage = 50_000
    colour = "blue"

for car in (Dodge, Chevy, Ford):
    print(f"The {car.__name__} has a top speed of {car.speed}, {car.mileage} miles and is {car.colour}")

